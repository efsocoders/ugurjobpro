package com.kolege.ugurjobpro;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

public class ArticleMain extends AppCompatActivity {

    private static final String urlWeb = "https://www.wired.com/";
    Context context = this;
    String title;
    TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_title = (TextView) findViewById(R.id.title);

        new AsyncWebArticleTitle().execute(urlWeb);


    }

    private class AsyncWebArticleTitle extends AsyncTask<String,String,String>{

        String htmlDatas;
        ProgressDialog progressDialogLoading;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialogLoading = new ProgressDialog(context);
            progressDialogLoading.setMessage("Loading...");
            progressDialogLoading.show();
        }

        @Override
        protected String doInBackground(String... url) {

            Document doc  = null;

            try {

                doc = Jsoup.connect(url[0]).get();
                Elements elements = doc.select("div[id=p1]");  // class ismi post-content olan verileri çekmek için
                Elements elText=elements.select("h2");
                String url1=elements.select("a").first().attr("abs:href");
                Log.e("Data", elements.toString());
                Log.e("Data", url1);
                htmlDatas = elements.text();

                title = doc.title();



            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            tv_title.setText(htmlDatas);
            progressDialogLoading.dismiss();
        }
    }
}
